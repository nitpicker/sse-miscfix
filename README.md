# MiscFix NetScriptFramework plugin

MiscFix plugin to workaround heavy SpellItem effect casting caused crash (BSTaskPool type 0x37).

## Requirements

* [Visual Studio 2019](https://visualstudio.microsoft.com/)
* [.NET Script Framework](https://www.nexusmods.com/skyrimspecialedition/mods/21294)

## Credit
* Developers and contributors for [SKSE64](https://skse.silverlock.org)
* Ryan-rsm-McKenzie and contributors for [SSE CommonLib](https://github.com/Ryan-rsm-McKenzie/CommonLibSSE) 
* Meh321 for [NET Script Framework](https://www.nexusmods.com/skyrimspecialedition/mods/21294)
