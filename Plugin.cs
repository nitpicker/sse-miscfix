﻿using NetScriptFramework;
using NetScriptFramework.SkyrimSE;
using NetScriptFramework.Tools;
using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace MiscFix
{
    public class Plugin : NetScriptFramework.Plugin
    {
        public override string Key => "plugin.miscfix";
        public static string PluginName => "miscfix";
        public override string Name => PluginName;
        public override int Version => 1;

        public override string Author => "Author";
        public override string Website => "https://github.com/crashme/";

        public override int RequiredFrameworkVersion => 9;
        public override int RequiredLibraryVersion => 14;
        private bool isInit = false;

        protected override bool Initialize(bool loadedAny)
        {
            //this.initTesting();
            this.putLockBeforeLatentSpellCast();
            this.check_hkp3AxisSweep_vfunc9_Node();
            this.check_EPMagic_SpellHasSkill_Null();
            //this.patch_AlwaysOsMalloc();

            //Events.OnMainMenu.Register(OnMainMenu, 0, 0, EventRegistrationFlags.None);
            return true;
        }
        private void OnMainMenu(MainMenuEventArgs e)
        {
            if (this.isInit)
                return;
            if (e.Entering)
            {
                // only works with local modified NetScriptFramework
                NetScriptFramework.Main.IsInitializing = true;
                isInit = true;
                this.check_DAR_unsafecasting();
                NetScriptFramework.Main.IsInitializing = false;               
            }
        }

        private void patch_AlwaysOsMalloc()
        {
            var addr_g_memoryAllocatorForceMalloc = NetScriptFramework.Main.GameInfo.GetAddressOf(523627, 0, 0, "");
            Memory.WriteBytes(addr_g_memoryAllocatorForceMalloc, new byte[] { 0x01 });

            var addr_g_onFreeCheckForSmallerAllocator = NetScriptFramework.Main.GameInfo.GetAddressOf(66861, 0x5C, 0, "48 83 7B 28 00 75 08");
            Memory.WriteBytes(addr_g_onFreeCheckForSmallerAllocator, new byte[] { 0xE8, 0x13 }, true);
        }
        private static SpinLock spinlock0 = new SpinLock();
        //private bool lockTaken = false;
        private void putLockPair(IntPtr addr1, IntPtr addr2)
        {
            Memory.WriteHook(new HookParameters()
            {
                Address = addr1,
                IncludeLength = 5,
                ReplaceLength = 5,
                Before = ctx =>
                {
                    bool lockTaken = false;
                    //System.Threading.Monitor.Enter(lockTaken);
                    spinlock0.Enter(ref lockTaken);
                }
            });

            Memory.WriteHook(new HookParameters()
            {
                Address = addr2,
                IncludeLength = 5,
                ReplaceLength = 5,
                Before = ctx =>
                {
                    //System.Threading.Monitor.Exit(lockTaken);
                    spinlock0.Exit();
                }
            });

        }
        private void putLockBeforeLatentSpellCast()
        {
            var addr_beforeGetActiveEffectList = NetScriptFramework.Main.GameInfo.GetAddressOf(33743, 0x10, 0, "0F 29 7C 24 30");
            var addr_epilogue = NetScriptFramework.Main.GameInfo.GetAddressOf(33743, 0x258, 0, "0F 28 7C 24 30");

            //var addr2_beforeGetActiveEffectList = NetScriptFramework.Main.GameInfo.GetAddressOf(33756, 0x10, 0, "FF 50 38");
            //var addr2_epilogue = NetScriptFramework.Main.GameInfo.GetAddressOf(33756, 0x4A, 0, "48 8B 5C 24 30");

            putLockPair(addr_beforeGetActiveEffectList, addr_epilogue);
            //putLockPair(addr2_beforeGetActiveEffectList, addr2_epilogue);
        }

        private void check_hkp3AxisSweep_vfunc9_Node()
        {
            // hkp3AxisSweep::Func9_B2CF00+2DE
            var addr = NetScriptFramework.Main.GameInfo.GetAddressOf(63685, 0x2DE, 0, "C7");

            Memory.WriteHook(new HookParameters()
            {
                Address = addr,
                IncludeLength = 6,
                ReplaceLength = 6,
                Before = ctx =>
                {
                    IntPtr phandle = ctx.AX;
                    IntPtr target = IntPtr.Zero;

                    if (phandle == IntPtr.Zero)
                    {
                        ctx.Skip(); // Skip: hkpBpNode handle = 0;
                        return;
                    }

                    if (Memory.TryReadPointer(phandle, ref target))
                    {
                        //Memory.WriteInterlockedUInt32(phandle, 0);
                        //ctx.Skip();
                        return;
                    }
                    else {
                        NetScriptFramework.Main.Log.AppendLine("hkp3AxisSweep::Func9+2DE bad hkpBpNode_handle :" + phandle.ToString("X"));
                    }

                }
            });
        }

        private void check_EPMagic_SpellHasSkill_Null()
        {
            // GameFunc::native::EPMagic_SpellHasSkill_2E28C0+31
            var addr = NetScriptFramework.Main.GameInfo.GetAddressOf(21295, 0x31, 0, "48 8B 48 10 ");

            Memory.WriteHook(new HookParameters()
            {
                Address = addr,
                IncludeLength = 0,
                ReplaceLength = 9,
                Before = ctx =>
                {
                    IntPtr ret = ctx.AX;
                    IntPtr pRef = IntPtr.Zero;

                    ctx.IP = addr - 0x31 + 0x4E;
                    if (ret == IntPtr.Zero)
                        return;
                    
                    if (Memory.TryReadPointer(ret+0x10, ref pRef))
                    {
                        Int32 ActorValue = Memory.ReadInt32(pRef + 0x78);
                        if (ActorValue != (Int32)ctx.DI)
                        {
                            ctx.IP = addr - 0x31 + 0x44; // Return 0.0f
                        } else
                        {
                            ctx.IP = addr - 0x31 + 0x3A; // Return 1.0f
                        }                    
                    }
                }
            });           
        }

        // DAR assumes unsafely IAnimationGraphManagerHolder is always derived from TESObjectREFR.
        // not always true ?!
        private void check_DAR_unsafecasting()
        {
            NetScriptFramework.Main.Log.AppendLine("check_DAR_unsafecasting...");
            var DARbase = MiscFix.Utils.GetModuleHandle("DynamicAnimationReplacer.dll");
            if (DARbase != IntPtr.Zero)
            {
                var addr = DARbase + 0xFC00 + 0x1E;
                var bytes = Memory.ReadBytes(addr, 1);
                if (bytes[0] == 0xE8)       // call 
                {
                    Memory.WriteHook(new HookParameters()
                    {
                        Address = addr,
                        IncludeLength = 0,
                        ReplaceLength = 5,
                        Before = ctx =>
                        {
                            var funcAddr = DARbase + 0xF890;
                            var thisPtr = ctx.CX;
                            IntPtr outValue = IntPtr.Zero;
                            //TESObjectREFR refobj = MemoryObject.FromAddress<TESObjectREFR>(thisPtr);
                            //if (refobj != null && refobj.IsValid)
                            //NetScriptFramework.Main.WriteNativeCrashLog(ctx, int.MinValue, "Data/NetScriptFramework/Plugins/MyPlugin_debug.txt");
                            if (Memory.TryReadPointer(thisPtr-0x38,ref outValue))
                            {
                                Memory.InvokeThisCall(thisPtr, funcAddr);
                            } else
                            {
                                NetScriptFramework.Main.Log.AppendLine("check_DAR_unsafecasting bad TESObjectREFR " + thisPtr.ToString("X"));

                            }
                        }
                    });
                    NetScriptFramework.Main.Log.AppendLine("check_DAR_unsafecasting writeHook @ " + addr.ToString("X"));
                }
            } else
            {
                NetScriptFramework.Main.Log.AppendLine("Can't find DAR image base");
            }

        }

        private void initTesting()
        {
            var addr = NetScriptFramework.Main.GameInfo.GetAddressOf(35986, 0x32, 0, "48 89 54 24 28");
            var addr2 = NetScriptFramework.Main.GameInfo.GetAddressOf(35986, 0x44, 0, "E8 17 37 00 00");
        
            IntPtr old_effect = IntPtr.Zero;

            Memory.WriteHook(new HookParameters()
            {
                Address = addr2,
                IncludeLength = 5,
                ReplaceLength = 5,
                Before = ctx =>
                {

                    IntPtr value = ctx.DI;
                    IntPtr target = IntPtr.Zero;
                    bool isMemory = false;

                    //NetScriptFramework.Main.WriteNativeCrashLog(ctx, int.MinValue, "Data/NetScriptFramework/Plugins/MyPlugin_debug.txt");

                    if (Memory.TryReadPointer(value, ref target))
                        isMemory = true;

                    if (isMemory)
                    {
                        
                        string types = NetScriptFramework.NativeCrashLog.GetValueInfo(value);

                        if (types.IndexOf("ScriptEffect") == -1)
                        {
                            return;
                        }
                        
                        old_effect = value;

                        NetScriptFramework.Main.Log.AppendLine("effect:  " + old_effect.ToString("X"));
                    }
                }             
            });
            //NetScriptFramework.Main.Log.AppendLine("init done");
        }
    }
}
